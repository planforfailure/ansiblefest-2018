![ansible](ansible_automation.png "ansible")


#### Overview

In this presentation, attendees will learn the basics of Ansible and get a thorough introduction to this automation and configuration management tool. What makes Ansible different? How does it work? These questions and more will be covered in this talk for anyone interested in learning Ansible from the ground up.

In this session, you will learn:

- What Ansible can do
- The technical aspects of how it works
- How to run playbooks and ad-hoc commands
- A quick tour of Ansible Tower

##### Offline Video
https://drive.google.com/file/d/1JYdSW5ky8wVipak6apfYB6AXK_XfgjDr
