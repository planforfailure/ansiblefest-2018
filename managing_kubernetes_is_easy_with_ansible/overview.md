![demo architecture](demo_architecture.png "Demo")

#### Overview

https://github.com/willthames/ansiblefest2018

https://willthames.github.io/ansiblefest2018/#/

Ansible is particularly well-suited to managing Kubernetes thanks to several core features:

- Templating 
- Secrets management
- Hierarchical inventory
- Lookup plugins and modules

This session will describe patterns and anti-patterns associated with managing Kubernetes configuration, plus a live demonstration configuring a Kubernetes environment from zero to deploying upgrades to applications without downtime.

##### Offline Video
https://drive.google.com/file/d/1r7mmgMNt7qxiZ4DTHaTcLjIvLLmIssvL/view
