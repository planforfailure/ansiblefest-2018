#### Overview

From not knowing what a playbook is to asking what a handler is, this talk goes over all of the basics and knowledge that you will need to know in order to write your first playbook. We'll walk through writing a playbook step by step.

In this session, you will learn: 

- What a playbook is
- The structure and contents of a playbook
- Where to find playbook and module resources
- Playbooks + Ansible Tower

##### Offline Video
https://drive.google.com/file/d/1Ld1NHYLqFG3b498N0fpS-Vb6ApVFgXRX
