#### Overview

Google has been autogenerating Ansible modules in order to provide a more consistent, comprehensive, and cohesive Ansible experience. We’ve built out over 40 modules (with facts modules!) in a short period of time using this approach. We’ll be doing a deep dive into the code generator we’ve built and how we approach building out Ansible modules that are fine-tuned towards specific cloud resources.

In this session, you will learn: 

- About Google’s approach to auto-generating Ansible modules and the tooling we’ve built out to create these modules at scale
- How Google has made this approach viable and pitfalls we’ve run into

##### Offline Video
https://drive.google.com/file/d/1pfwvLQI1aKQHf7g_t3a9I0b5wL_WlCuR
