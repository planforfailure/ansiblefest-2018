#### Overview

The National Environmental Satellite Data Information Service (NESDIS) reduced their costs for RHEL configuration management by 35 percent by migrating from Puppet Enterprise to Ansible Tower. In this how-we-did-it session, the lead implementor of that migration presents the rationale for doing the migration, tips for translating a Puppet manifest to Ansible playbooks, and valuable lessons learned along the way.

In this session, you will learn: 

- How to make the case to management for migrating from Puppet Enterprise to Ansible Tower
- Strategies to make your migration go smoothly
- Tips for transcoding PE manifests into Ansible playbooks and roles
- Best practices for a smooth-running Tower installation


##### Offlne Video

https://drive.google.com/file/d/1opB65AABkrXRuNrL0svX7a0zZLzopL2z/
