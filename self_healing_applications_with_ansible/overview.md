#### Overview

Although developers strive to build robust applications, not only the applications but also the underlying infrastructure is not immune to failures. Therefore, monitoring the infrastructure and applications is inevitable, but also providing automated, real-time remediation for detected problems is crucial for successfully running your applications. 

In this session we will explore how Ansible can be leveraged for building auto-remediation workflow to enable self-healing applications.

##### Download Video
https://drive.google.com/file/d/13kNqCyMQudMBaeUeTRs5pM236mRImjZ_/view
