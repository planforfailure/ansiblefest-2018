#### Overview

An unwritten rule in the world of IT Operations is that you never make changes on a Friday afternoon, especially not upgrading the backend database for a business e-commerce site contributing a £2 billion revenue. Ansible Tower makes the unthinkable thinkable by producing reliable, consistent, repeatable results, and removing the possibility of human error from activities. Sports Direct in the UK were so impressed with the performance of Ansible that they felt confident enough to do just this!

In this session, you will learn: 

- How Ansible combined with a rigorous test cycle can produce reliable, repeatable results
- How Ansible can increase confidence in your IT operations and empower admins whilst making their jobs easier and more efficient
- How Ansible can help reduce downtime as part of a good IT processes
- How Ansible Tower and AWX provide rich ACL's to enable good control over infrastructure changes
- How changes can be made more reliable and repeatable with Ansible

##### Offline Video
https://drive.google.com/file/d/1ZApfkJHFYoeKWGdaPUFMNHhmUDZ2YhqR
