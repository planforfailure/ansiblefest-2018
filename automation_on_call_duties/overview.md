![on call](on_call_ansible_tower.png "Demo")

#### Overview

https://github.com/ansible/tower-nagios-integration


In this presentation, Lauren and Mauricio talk about how they automated the on-call procedure for the operations team in IT at Red Hat. Instead of paging the on-call person, when a host alerts, Nagios (the IT monitoring service) calls the Ansible Tower API for the specific alert. Then a playbook (automated with Ansible) runs. The alert then clears and posts a blog to the Red Hat intranet making the on-call team aware. A post also goes to IRC. If for some reason Ansible fails to fix the paging host, the on-call person will then be paged. 

##### Offline Video
https://drive.google.com/file/d/1x_JL7BuvdDSQhHbZ9tRI0HsbGfJO_1Yw
