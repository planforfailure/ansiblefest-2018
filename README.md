# Ansiblefest18

All the presentations with the slides, summaries and offline videos of all the talks.

- [Managing Kubernetes is Easy with Ansible](managing_kubernetes_is_easy_with_ansible/overview.md)
- [Self-healing Applications with Ansible](self_healing_applications_with_ansible/overview.md)
- [Network Automation at Scale - Managing 15,00 Network Devices](network_automation_at_scale/overview.md)
- [Automate Windows Environments with Ansible and DSC](automate_windows_environments/overview.md)
- [Automating OnCall Duties for Red Hat IT with Ansible and Ansible Tower](automation_on_call_duties/overview.md)
- [Giving Power to the People with Ansible at General Mills](ansible_at_general_mills/overview.md)
- [Using Ansible Tower to Implement Security Policies and Telemetry Streaming for Hybrid Clouds](ansible_tower_security_principles/overview.md)
- [The DevOps Opportunity: Balancing Security and Velocity](devops_balancing_security_velocity/overview.md)
- [Introducing Ansible Network Engine Role for Network Automation](network_engine_role_with_network_automation/overview.md)
- [Upgrading the Backend Database of a £3 Billion Business Website on a Friday Afternoon with Ansible](upgrading_the_backend_database_friday_afternoon/overview.md)
- [Writing Your First Playbook](writing_your_first_playbook/overview.md)
- [Ansible Essentials](ansible_essentials/overview.md)
- [Cutting the Strings: Migrating from Puppet Enterprise to Ansible Tower](cutting_the_strings_from_puppet_enterprise_tower/overview.md)
- [Auto-Generating Google Cloud Ansible Modules](auto_generating_google_cloud_modules/overview.md)
