#### Overview

Network analytics provides insight to the traffic flow between applications and endpoints. Telemetry data is streamed in real-time from software sensors and network devices to big-data clusters. Implementing the policy to create a whitelist-based segmentation and zero-trust model requires automation when dealing with tens of thousands of workloads and complex rules. 

This session examines how Cisco Tetration Analytics provides an accurate inventory of devices, software packages and version information to detect software vulnerabilities and implement a zero-trust policy model on network fabrics, firewalls and application delivery controllers. 

##### Offline Video
https://drive.google.com/file/d/15eEO9NfgudtBpDgTx6SpvxSkmHl5q_d0/view
