#### Overview

https://github.com/infamousjoeg/cyberark-ansible-lamp


Organizations are in a never-ending race to deliver revolutionary applications, technology and services to improve the customer experience and drive overall growth. Fortunately, the adoption of DevOps is accelerating innovation, allowing developers to achieve unparalleled velocity. But are organizations equipped to handle the security risk that can accompany DevOps speed? This session will explore how one of the world’s largest banks did exactly that.

In this session, attendees will learn how Ansible and CyberArk were used to modernize the orchestration of new services and infrastructure for mission-critical applications. Based on real-world use cases, attendees will benefit from practical guidance and techniques shared from the front lines of security, development and operations to successfully modernize the orchestration of new services in their own organizations.

##### Offline Video
https://drive.google.com/file/d/1OoVasDNfeqLYpDofGxOYNKClxQCPcS8t
