![windows](win_ansible.png "windows")

#### Overview

https://www.ansible.com/integrations/infrastructure/windows

https://github.com/oatakan?tab=repositories

Ansible provides a robust set of modules to manage Windows environments. When you combine the power of Ansible with the Desired State Configuration resources, you can create automation that can massively scale and manage your Windows environment. In this session you will see how you can utilize DSC resources with Ansible. 

##### Offline Video
https://drive.google.com/file/d/1w7ZmlzHwOmg5MB-dWFVToqLqTIDsIYlg/view
