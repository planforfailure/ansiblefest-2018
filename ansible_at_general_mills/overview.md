![general mills](general_mills.png "general mills")

#### Overview

Find out why General Mills has made Ansible their automation tool of choice. It's currently used by more than 15 teams - in a number of ways - including automating a process that used to involved eight people, but now is done at the push of a button with end-to-end infrastructure integration.

In this session, you will learn: 

- How we went from Ansible to Red Hat Ansible Tower
- How we reused and review playbooks across the company
- How our non-tech teams do tech things and our users automate when they don't code

##### Offline Video
https://drive.google.com/file/d/1HLkmJS3ouynVtj8diTCFp7qahV-JOPS3/view
