![network automation](network_automation.png "Demo")

#### Overview

[network_automation_at_scale.pdf](network_automation_at_scale.pdf)

Ansible allows network management across virtually any device platform. Any network device can be managed via SSH or an API. We took Ansible's cutting-edge network automation to scale with a customer's global network infrastructure, giving them the ability to manage nearly all of their network devices at one time.


![network automation](networking_at_scale.png "Demo")

![network automation](networking_at_scale2.png "Demo")

![network automation](networking_at_scale3.png "Demo")


##### Offline Video
https://drive.google.com/file/d/1yIMVDlZ2oJRElbPywN6Sc8tSwGDLKw4T
