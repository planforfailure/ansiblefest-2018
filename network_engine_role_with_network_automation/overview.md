#### Overview

The Network-Engine role available in Ansible Galaxy provides a set of consumable functions to automate bootstrapping, provisioning and configuration management of network infrastructure. The role is a bunch of configuration and operational plugins that are specific for network automation. It provides the foundation for building network roles by providing modules and plugins that are common to all Ansible Network roles. The role supports truly platform-agnostic network automation.

In this session we will know what Network Engine is, where does the role fit in the Network Automation and what are the configuration and operational plugins the role provides, followed by a demo.

###### Offline Video

https://drive.google.com/file/d/1f5xj_KmbDvohmAc-MC8fs4fuWlj9BUa-/
